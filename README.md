# Pulumi Unit testing example

## Step 1: Instantiate definitions
For testing Pulumi inputs you need to instantiate your classes somewhere. For example, if you have a class called Graphana, you will need to instantiate it somewhere. If you are working in a definitions-only project like pulumi-shared. Then you should create a file called graphana.scenario.ts in a directory called test, and instantiate your class there: for example something like this:

~~~
const metallb = new Metallb(
 'metallb',
 {},
 {
 addressPool: ['192.168.1.1-192.168.2.1'],
 }
);
~~~

## Step 2: Create the tests:
After you instantiated your classes that you want to test, then create another file with extension .test.ts in the tests directory that you created  previously. And have in mind the next considerations:

### Import pulumi and mocha:
~~~
import * as pulumi from '@pulumi/pulumi';
import 'mocha';
~~~

### Create pulumi mocks like this:
~~~
pulumi.runtime.setMocks({
 newResource(args: pulumi.runtime.MockResourceArgs): {
  id: string | undefined;
  state: Record<string, any>;
 } {
  return {
   id: `${args.inputs.name  }_id`,
   state: args.inputs,
  };
 },
 call(args: pulumi.runtime.MockCallArgs): Record<string, any> {
  return args.inputs;
 },
});
~~~

### Create the tests using de following pattern:
~~~
describe('MetalLB', () => {
 let scenario: typeof import('./metallb.scenario');

 before(async () => {
  scenario = await import('./metallb.scenario');
 });

 it(' should not have an empty configMap', (done) => {
  pulumi.all([scenario.metallb.urn, scenario.metallb.configMap.data]).apply(([urn, data]) => {
   if (!data.config) {
    done(new Error(`Empty configMap in Metallb ${urn}`));
   } else {
    done();
   }
  });
 });
});
~~~
Have in mind that you need to import your file with the instantiated resources in the "before" block, otherwise your tests will not work.

Also you need to have in mind that you must access the data resolving it with pulumi.all().apply().

## Step 3: Run the tests
For running your tests you should use your IDE or run the next command in a terminal at the root directory of your project:
~~~
yarn run test
~~~

## Known Bugs and Issues:
### Timeout Exceeded
#### Issue
~~~
Error: Timeout of 2000ms exceeded. For async tests and hooks, ensure "done()" is called; if returning a Promise, ensure it resolves.
~~~
#### Fix
If you are seeing this, you dont have pulumi 3.9.1 or you dont have "resolutions" section in your package.json

### Rerun using pulumi cli
#### Issue:
~~~
Error: Program run without the Pulumi engine available; re-run using the `pulumi` CLI
~~~
#### Fix:
If you are seeing this, you missed to import the scenario at the "before" block.